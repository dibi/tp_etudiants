<?php
class Poneys
{
	private $count;

	public function getCount()
	{
		return $this->count;
	}

	public function setCount($a)
	{
		$this->count = $a;
	}

	public function removePoneyFromField($number)
	{
		if ($number > $this->count)
		{
			throw new Exception("Cannot remove more poneys than available", 1);
		}
		else
		{
			$this->count -= $number;
		}
	}

	public function addPoneyFromField($number)
	{
		$this->count += $number;
	}

	public function getNames()
	{

	}

	public function hasFreeSpace()
	{
		if ($this->count < intval(getEnv("BASEFIELDSIZE")))
			return TRUE;
		else
			return FALSE;
	}
}
?>
