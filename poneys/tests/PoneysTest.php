<?php
require_once 'src/Poneys.php';

class PoneysTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->Poneys = new Poneys;
		$this->Poneys->setCount(intval(getEnv("BASEPONEYSNUMBER")));
	}

	public function tearDown()
	{
		unset($this->Poneys);
	}

	/**
	 * @dataProvider removePoneyFromFieldProvider
	 */
	public function test_removePoneyFromField($number, $expected)
	{
		$this->Poneys->removePoneyFromField($number);
	}

	public function removePoneyFromFieldProvider()
	{
		return [
			[0, 8],
			[1, 7],
			[-6, 14]
		];
	}



	public function test_addPoneyFromField()
	{
		$this->Poneys->addPoneyFromField(2);
		$this->assertEquals(10, $this->Poneys->getCount());
	}

	/**
	 * @expectedException Exception
	 */
	public function test_removeTooMuchPoneyFromField()
	{
		$this->Poneys->removePoneyFromField(10);
	}

	public function test_getNames()
	{
		$stub = $this->getMock(Poneys::class);
		$stub->method('getNames')
			 ->willReturn(array("1","2","3"));
		$this->assertEquals(array("1","2","3"), $stub->getNames());
	}

	public function test_freeSpace()
	{
		$this->assertTrue($this->Poneys->hasFreeSpace());

		$this->Poneys->addPoneyFromField(7);

		$this->assertFalse($this->Poneys->hasFreeSpace());
	}
}
?>
