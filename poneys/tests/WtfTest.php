<?php
require_once 'src/Poneys.php';

class WtfTest extends \PHPUnit_Framework_TestCase
{
	public function setUp()
	{
		$this->Poneys = new Poneys;
		$this->Poneys->setCount(intval(getEnv("BASEPONEYSNUMBER")));
	}

	public function tearDown()
	{
		unset($this->Poneys);
	}

	public function test_addPoneyToField()
	{
		$this->Poneys->addPoneyFromField(2);
		$this->assertEquals(10, $this->Poneys->getCount());
	}

}
?>
